# Computational Neuroscience
### 2nd Assignment - Shahid Beheshti University - Bachelor’s Program
March 14, 2023<br/> 
Mahdieh Sajedi Pour, Nima Taheri<br/>

## Exercise 1
In this part, you will create a population with 15 neurons and feed them with three different currents; a constant current, a sine current, and a steps current. You have to do this in 4 different ways:<br/>
a. Full connectivity with scaling <br/>
b. Full connectivity with Gaussian distribution <br/>
c. Random connectivity with fixed coupling probability <br/>
d. Random connectivity with fixed number of presynaptic partners<br/>

Then, print the raster plot for each of them and compare.

## Exercise 2
Consider two excitatory neuron populations and one inhibitory neuron population. Choose the number of neurons in the population as desired. Establish a connection between each pair of populations (inner population connections must be established using one of the methods in the previous question). Now, consider a constant current and add two noises of the same tone to this current in 50-millisecond intervals (the amount of noise that is added is a constant value in the entire interval) to obtain two different currents. Feed the two excitatory populations with these two currents. Repeat this process for 5 times so that the whole period would be 250 milliseconds. Now, draw the raster plot of the two populations and check the effect of the inhibitory population on their behavior according to the currents. Finally, describe the role of the inhibitory populations in decision-making.

## Exercise 3
Consider 5 constant currents and design a neural population for each of the following encoding methods. Feed each neuron with every current for a sufficient length of time (according to the parameters of neurons and populations as much as it can be encoded) and report the final result.<br/>
● Poisson encoding<br/>
● Time to first spike<br/>
● Rank order coding<br/>
